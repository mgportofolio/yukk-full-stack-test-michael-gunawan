<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table = 'transaction';

    protected $fillable = [
        'user_id',
        'transaction_name',
        'transaction_type',
        'description',
        'amount',
    ];

    // Add any additional model methods or relationships here
}
