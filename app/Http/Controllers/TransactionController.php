<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\Transaction;

class TransactionController extends Controller
{
    //
    public function index()
    {
        $user = auth()->user();
        $transactions = Transaction::where('user_id', $user->id)->paginate(10);
        $balance = Transaction::where('user_id', $user->id)->sum('amount');
        return Inertia::render('Dashboard', [
            'transactions' => $transactions,
            'balance' => $balance
        ]);
    }

    public function topup()
    {
        $user = auth()->user();
        $balance = Transaction::where('user_id', $user->id)->sum('amount');
        return Inertia::render('TopUp', [
            'balance' => $balance
        ]);
    }

    public function purchase()
    {
        $user = auth()->user();
        $balance = Transaction::where('user_id', $user->id)->sum('amount');
        return Inertia::render('Purchase', [
            'balance' => $balance
        ]);
    }
    
    public function search(Request $request){
        $queryParam = $request['search'];
        $query = Transaction::query();
        $user = auth()->user();

        $query->where('user_id', $user->id);

        $query->where(function ($subquery) use ($queryParam) {
            $subquery->where('transaction_name', 'LIKE', '%' . $queryParam . '%')
                    ->orWhere('transaction_type', 'LIKE', '%' . $queryParam . '%')
                    ->orWhere('description', 'LIKE', '%' . $queryParam . '%');
        });

        $transactions = $query->paginate(10);
        $balance = Transaction::where('user_id', $user->id)->sum('amount');
        return Inertia::render('Dashboard', [
            'transactions' => $transactions,
            'balance' => $balance
        ]);
    }

    public function transaction(Request $request)
    {
        $user = auth()->user();
        try {
            $validatedData = $request->validate([
                'transaction_name' => 'required',
                'transaction_type' => 'required',
                'amount' => 'required|numeric',
                'description' => 'required'
            ]);
            $record = new Transaction();
            $record->transaction_name = $validatedData['transaction_name'];
            $record->transaction_type = $validatedData['transaction_type'];
            $record->amount = $validatedData['amount'];
            $record->description = $validatedData['description'];
            $record->user_id = $user->id;
            $record->save();
            return redirect()->route('dashboard');
        }catch(ValidationException $e){
            return back()->withErrors($e->validator)->withInput();
        }
    }
}
